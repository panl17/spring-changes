FROM maven:3.6.3-openjdk-11 AS compile
COPY . /usr/src/mymaven
WORKDIR /usr/src/mymaven
RUN mvn -Dmaven.test.skip=true clean package



FROM openjdk:11
RUN wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java_8.0.24-1debian9_all.deb -O /tmp/mysql-connector.deb
RUN dpkg -i /tmp/mysql-connector.deb
COPY --from=compile /usr/src/mymaven/target/spring-petclinic-2.4.5.jar app.jar
COPY src/main/resources/application-docker.properties application.properties
EXPOSE 8080
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/urandom -jar /app.jar" ]


RUN dnf -y install curl wget
RUN wget https://packagecloud.io/install/repositories/prometheus-rpm/release/script.rpm.sh -O /tmp/prom.rpm.sh
RUN chmod +x /tmp/prom.rpm.sh
RUN os=el dist=8 /tmp/prom.rpm.sh
RUN dnf -y install node_exporter
RUN dnf -y install prometheus
COPY prometheus.yml /etc/prometheus/prometheus.yml
RUN mkdir /app